<?php

$response['data'] = [
    [
        "DT_RowId"=> "row_1",
        'name'=>'John',
        'email'=>'john@gmail.com',
        'phone'=>'9123491234',
        'address'=>'1 Stevan Street, New York',
    ],[
        "DT_RowId"=> "row_2",
        'name'=>'Jenny',
        'email'=>'jenny@gmail.com',
        'phone'=>'9123491234',
        'address'=>'10 Marvon Street, New York',
    ]
]; 
print_r(json_encode($response));
